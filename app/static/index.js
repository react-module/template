require.config({
  paths: {
    "text": "libs/text/text",
    "jquery": "libs/jquery/dist/jquery.min",
    "director": "libs/director/build/director.min",
    "backbone": "libs/underscore/underscore.min",
    "bootstrap": "libs/bootstrap/dist/js/bootstrap.min"
  },
  shim: {
    bootstrap: {
      deps: ['jquery']
    }
  }
});
require(['app/page'], function(page) {
  new page.Page('#page');

  $('#side-folder').click(function() {
    $('body').toggleClass('fold');
  });

  $('.fold .nav-item').mouseover(function(e) {
    $(e.target).addClass('active');
  }).mouseout(function(e) {
    $(e.target).removeClass('active');
  });


});
