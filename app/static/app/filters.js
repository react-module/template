define([
  'jquery', 'base/events', 'auth/signin'
], function($, events, signin) {
  "use strict";

  var isLogin = true;


  var auth = function() {
    return isLogin;
  }


  return {'auth': auth}

});
