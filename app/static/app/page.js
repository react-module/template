define([
  'jquery', 'base/events', 'base/router', '../handler'
], function($, events, router, handler) {
  "use strict";

  function Page() {
    this.initialize.apply(this, arguments);
  }

  Page.prototype.initialize = function() {

    this.$ele = $('#page');
    var ele = arguments['ele'];
    if (ele) {
      this.$ele = $(ele);
    }
    this.router = router.Route.init(handler);
    this.$ele.css('display', 'block');
    this.bind_events();
  }

  Page.prototype.bind_events = function() {
    // resize site on:
    // - window resize
    // - header change
    // - page load
    _resize_handler.call(this, _resize_handler);
  };

  var _resize_handler = function() {
    var _handle_resize = $.proxy(this._resize, this);

    $(window).resize(_handle_resize);
    $(document).ready(_handle_resize);
    events.on('page.resize', _handle_resize);
  }


  Page.prototype._resize = function(e) {
    /**
    * Update the site's size.
    */

    // In the case an event is passed in, only trigger if the event does
    // *not* have a target DOM node (i.e., it is not bubbling up). See
    // https://bugs.jquery.com/ticket/9841#comment:8
    if (!(e && e.target && e.target.tagName)) {
      // $('div#site').height($(window).height() - $('#header').height());
      this.$ele.height($(window).height());
    }
  };

  return {'Page': Page};
});
