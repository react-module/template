define([
  'jquery', 'base/events', 'auth/signin'
], function($, events, signin) {
  "use strict";

  var Events = {
    init: function() {
      signin.Signin.html();
    }
  }

  return {'Events': Events}

});
