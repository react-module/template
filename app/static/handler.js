define(function(require) {

  var events = require('base/events');

  var root = function() {
    console.log('jjjjjjjj');
  }
  var zoo = function() {
    console.log('zzzzzz');
  }
  var login = function() {
    events.trigger('auth.signin');
  }

  var routes = {
    '/': require('auth/signup'),
    '/auth': require('auth/signin'),
    '/root': root,
    '/zoo': zoo,
    '/login': login
  };

  return routes;
});
