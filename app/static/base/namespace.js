var Ipenta = Ipenta || {};

var jprop = function(name, module_path) {
  Object.defineProperty(Ipenta, name, {
    get: function() {
      console.warn('accessing `' + name + '` is deprecated. Use `require("' + module_path + '")`');
      return require(module_path);
    },
    enumerable: true,
    configurable: false
  });
}

var jglobal = function(name, module_path) {
  Object.defineProperty(Ipenta, name, {
    get: function() {
      console.warn('accessing `' + name + '` is deprecated. Use `require("' + module_path + '").' + name + '`');
      return require(module_path)[name];
    },
    enumerable: true,
    configurable: false
  });
}

define(function() {
  "use strict";

  Ipenta.version = "0.0.1.dev";
  Ipenta._target = '_blank';
  return Ipenta;
});
