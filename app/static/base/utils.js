define(function() {
  var isJson = function(obj) {
    var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length;
    return isjson;
  }

  function fix() {
    Function.prototype.before = function(beforfunc) {
      var self = this;
      return function() {
        beforfunc.apply(this, arguments);
        self.apply(this, arguments);
      };
    };

    Function.prototype.after = function(afterfunc) {
      var self = this;
      return function() {
        self.apply(this, arguments);
        afterfunc.apply(this, arguments);
      };
    };
  }

  return {isJson: isJson, fix: fix}

});
