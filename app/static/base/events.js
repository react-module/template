define([
  'jquery', './namespace'
], function($, Ipenta) {
  "use strict";

  // Events singleton
  if (!window._Events) {
    window._Events = function() {};
    window._events = new window._Events();
  }

  // Backwards compatability.
  Ipenta.Events = window._Events;
  Ipenta.events = window._events;

  var events = $([window._events]);

  // catch and log errors in triggered events
  events._original_trigger = events.trigger;
  events.trigger = function(name, data) {
    try {
      this._original_trigger.apply(this, arguments);
    } catch (e) {
      console.error("Exception in event handler for " + name, e, arguments);
    }
  }
  return events;
});
