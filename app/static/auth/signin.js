define([
  "base/events", "text!./signin.html"
], function(events, template) {

  var controller = function() {
    $(".content").html(template);
  }

  return controller;
});
