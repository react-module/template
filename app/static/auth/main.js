define([
  './signin', './signup'
], function(signin, signup) {
  return {signin: signin, signup: signup};
});
